﻿// See https://aka.ms/new-console-template for more information
using UserDefinedInterfacesDemo;

List<IMoveable> moveableSprites = new List<IMoveable>();

moveableSprites.Add(new DodgeBall());
moveableSprites.Add(new Animal());
moveableSprites.Add(new Animal());
moveableSprites.Add(new DodgeBall());


Random rand = new Random();

foreach (IMoveable moveableSprite in moveableSprites)
{
    moveableSprite.Move(rand.Next(100), rand.Next(100));
}