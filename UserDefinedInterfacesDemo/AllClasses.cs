﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserDefinedInterfacesDemo
{
    //normally all of these classes would be seperate but the teacher wanted to show something

    //the interfaces would also have their own seperate interface file
    interface IMoveable
    {
        void Move(int xLoc, int yLoc);
    }

    interface IGrowable
    {
        void Grow(int xLoc, int yLoc);
    }
    abstract class Sprite { 
    
        public int XPosition { get; set; }
        public int YPosition { get; set; }
    
    }

    abstract class Obstacle : Sprite { 
    
        public int DamagePoints { get; set; }
    }

    abstract class Food : Sprite { 

        public int EnergyPoints { get; set; }
    }

    class Player : Sprite { }

    class DodgeBall : Obstacle, IMoveable
    {
        public void Move(int xLoc, int yLoc)
        {
            XPosition = xLoc;
            YPosition = yLoc;
            Console.WriteLine($"The dodgeball moves to {xLoc}, {yLoc}");
        }
    }

    class Animal : Food, IMoveable, IGrowable
    {
        public void Move(int xLoc, int yLoc)
        {
            XPosition = xLoc;
            YPosition = yLoc;
            Console.WriteLine($"The animal moves to {xLoc}, {yLoc}");
        }

        public void Grow(int xLoc, int yLoc)
        {
            XPosition = xLoc;
            YPosition = yLoc;
            Console.WriteLine($"The animal moves to {xLoc}, {yLoc}");
        }

    }

}
